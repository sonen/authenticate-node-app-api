const http = require('http');
const app = require('./app');
const mongoose = require('mongoose');
const config = require('./config');

const port = process.env.PORT || 3000;
mongoose.connect(config.database); // connect to database
//app.set('superSecret', config.secret); // secret variable

const server = http.createServer(app);

server.listen(port);
console.log('Sonen Node App at http://localhost:' + port);
const express = require('express');
const app = express();
const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('../../config');


app.set('superSecret', config.secret);
router.use((req, res, next) => {
    // Check header, url parameters or post parameters for a token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode the token
    if (token) {
        jwt.verify(token, app.get('superSecret'), (err, decoded) => {
            if (err) {
                res.json({
                    success: false,
                    message: 'Failed to authenticate token'
                })
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        return res.status(403).send({
            success: false,
            message: 'No token provided'
        })
    }
});

module.exports = router;
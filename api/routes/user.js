const express = require('express');
const router = express.Router();

const User = require('../models/user');

router.get('/setup', (req, res, next) => {

    let nick = new User({
        name: 'Nick Cerminara',
        password: 'password',
        admin: true
    });

    nick.save()
            .then(result => {
                console.log('Result', result)
                res.status(201).json({
                    message: 'User created successfully'
                });
            })
            .catch(err => {
                res.status(500).json({
                    error: err
                });
            });

});

router.get('/all-users', (req, res, next) => {

    User.find()
            .select()
            .exec()
            .then(doc => {
                const response = {
                    count: doc.length,
                    user: doc.map(doc => {
                        return {
                            _id: doc._id,
                            name: doc.name,
                            admin: doc.admin
                        }
                    })
                }


                if (doc) {
                    res.status(200).json(response);
                } else {
                    res.status(404).json({
                        message: 'No entry found'
                    });
                }

            })
            .catch(err => {
                res.status(500).json({
                    error: err
                });
            });

});


module.exports = router;
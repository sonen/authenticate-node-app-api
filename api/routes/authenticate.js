const express = require('express');
const app = express();
const router = express.Router();
const jwt    = require('jsonwebtoken');
const config = require('../../config');

const User = require('../models/user');

app.set('superSecret', config.secret);

router.post('/authenticate', (req, res, next) => {
    User.findOne({
        name: req.body.name
    }).exec()
            .then( user => {
                if (!user) {
                    res.status(404).json({
                        success: 'false',
                        message: 'Authentication Failed, User not found!'
                    });
                } else if (user) {
                    if (user.password != req.body.password) {
                        res.json({
                            success: 'false',
                            message: 'Authentication Failed, Wrong Password!'
                        });
                    } else {
                        // If the user is found and the password is right
                        // create a token with only our given payload
                        const payload = {
                            admin: user.admin
                        };
                        var token = jwt.sign(payload, app.get('superSecret'),{
                            expiresIn: 60
                        });

                        // return the information including the token as json
                        res.status(200).json({
                            success: 'true',
                            message: 'Enjoy your token',
                            token: token
                        });
                    }
                }
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err,
                    message: 'An error ocurred'
                });
            });
});

module.exports = router;
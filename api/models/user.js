// get an instance of mongoose and mongoose.Schema
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const userSchema = mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    name: { type: String },
    password: { type: String },
    admin: { type: Boolean }
});

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', userSchema);
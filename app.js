const express     = require('express');
const app         = express();
const bodyParser  = require('body-parser');
const morgan      = require('morgan');
const mongoose    = require('mongoose');

const jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('./config'); // get our config file
const User   = require('./api/models/user'); // get our mongoose model

const userRoutes = require('./api/routes/user');
const authenticateRoutes = require('./api/routes/authenticate');
const middlewareRoutes = require('./api/routes/middleware');

app.set('superSecret', config.secret);

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));


// Handling the CORS Issue
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Contron-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST , PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// The test route
app.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'The API test is Passed, everything working fine'
    });
});

// API ROUTES
app.use('/auth', authenticateRoutes);

// Route middleware to verify a token
app.use(middlewareRoutes);

app.use('/user', [middlewareRoutes], userRoutes);

// If the request cannot be handled by any of the route end points
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;